<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 11/05/2018
 * Time: 23:36
 */

// write the ucaml string to the ucaml file
$ucaml=toUcaml();
$file=fopen("../ucaml/ucaml.ucaml",'w+');
fwrite($file,$ucaml);
fclose($file);

//Put the ucaml into a temporary file and export the target platform format by M2M transformation.
$tmpfname = tempnam(sys_get_temp_dir(), "FOO");
$handle = fopen($tmpfname, "w");
fwrite($handle, $ucaml);
if($_GET["way"]==="Docker"){
    exec("ucaml.rb transform --docker ".$tmpfname,$result);

    //Put the result into txt file and download it in client side
    $txt="";
    for($i=0;$i<count($result);$i++){
        $txt=$txt.$result[$i]."\n";
    }
    downloadUcaml("../ucaml/gem.txt",$txt,"Docker.txt");
}
else{
    exec("ucaml.rb transform --k8s ".$tmpfname,$result);

    //Put the result into txt file and download it in client side
    $txt="";
    for($i=0;$i<count($result);$i++){
        $txt=$txt.$result[$i]."\n";
    }
    downloadUcaml("../ucaml/gem.txt",$txt,"Kubernetes.txt");
}



/**
 * Transform the json to the ucaml string and return it
 *
 * @return string ucaml string
 */
function toUcaml(){
    $info = $_POST;
    $json=json_encode($info,JSON_PRETTY_PRINT);
    $file=fopen('../json/form.json','w+');
    fwrite($file,$json);
    fclose($file);

    $json_string = file_get_contents('../json/form.json');
    $data=json_decode($json_string, true);

    $string= "Ucaml::application('".$data["name"]."',\n".parametersToUcaml($data,"  ")."services:[\n".serviceToUcaml($data["services"])."])";
    return $string;
}

/**
 * Transform the volume to ucaml format
 *
 * @param $data
 * @param $key
 * @return string
 */
function volumeToUcaml($data, $key){
    $mountPath="";
    $type=$data[$key]["type"];
    $a="";

    if($type=="nfs" || $type=="git" || $type=="secret"){
        $a=$type.":'".$data[$key][$type]."', ";
    }
    if($data[$key]["mountPath"]!==""){
        $mountPath="mountPath:'".$data[$key]["mountPath"]."'";
    }
    $temp="'".$data[$key]["name"]."', :".$type.", ".$a.$mountPath;
    return $temp;
}

/**
 * Transform each specific parameter to ucaml format
 *
 * @param $data
 * @param $space
 * @return string
 */
function parametersToUcaml($data, $space){
    $text=$space;
    foreach($data as $key => $value){
        $temp="";
        if($key=="labels" || $key=="environment" || $key=="capabilities"){
            for($a=0;$a<count($data[$key]["key"]);$a++) {
                $temp = $temp . $data[$key]["key"][$a] . ": " . "'".$data[$key]["value"][$a]."', ";
            }
            $temp=rtrim($temp, ", ");
            $text = $text . $key . ": Ucaml::" . $key . "(" . $temp . "),\n".$space;

        }

        if($key==="scheduling"){
            for($a=0;$a<count($data[$key]["key"]);$a++) {
                $temp = $temp ."'". $data[$key]["key"][$a] . "': " . "'".$data[$key]["value"][$a]."', ";
            }
            $temp=rtrim($temp, ", ");
            $text = $text . $key . ": Ucaml::constraint(" . $temp . "),\n".$space;
        }

        if($key=="volume"){
            $temp=volumeToUcaml($data,$key);
            $text = $text . "volumes" . ": [Ucaml::" . $key . "(" . $temp . ")],\n".$space;
        }

        if($key=="request" || $key=="scale"){
            if($key=="scale"){
                $a="scalingrule";
            }
            else{
                $a=$key;
            }
            foreach ($data[$key] as $scaleKey => $scaleValue){
                if($scaleValue!=="") {
                    $temp = $temp . $scaleKey . ": " . $scaleValue.", ";
                }
            }
            $temp=rtrim($temp, ", ");
            $text = $text . $key . ": Ucaml::" . $a . "(" . $temp . "),\n".$space;
        }

        if($key=="ports"){
            $temp=$text . $key .": [";
            for($i=0;$i<count($data["ports"]);$i++){
                $temp=$temp.$data["ports"][$i].", ";
            }
            $temp=rtrim($temp, ", ");
            $text = $temp."],\n".$space;
        }

        if($key=="expose"){
            for($a=0;$a<count($data[$key]["key"]);$a++) {
                $temp = $temp .$data[$key]["key"][$a] . " => " .$data[$key]["value"][$a]." , ";
            }
            $temp=rtrim($temp, ", ");
            $text = $text . $key .": [". $temp . "],\n".$space;
        }

        if($key=="container"){
            $cmd=" ,[";
            for($i=0;$i<count($data["container"]["cmd"]);$i++){
                $cmd=$cmd."\"".$data["container"]["cmd"][$i]."\", ";
            }
            if($data["container"]["cmd"][0]==="" || count($data["container"]["cmd"])===0){
                $cmd="";
            }
            else{
                $cmd=rtrim($cmd, ", ");
                $cmd=$cmd." ]";
            }

            $temp="'".$data["container"]["name"]."', '".$data["container"]["image"]."'".$cmd;
            $text = $text . $key . ": Ucaml::" . $key . "(" . $temp . "),";
        }
    }
    $text=rtrim($text, ", ");
    return $text;
}


/**
 * Transform all services to ucaml format
 *
 * @param $services
 * @return string
 */
function serviceToUcaml($services){
  $html="";
   for($i=0;$i<count($services);$i++){
       $service=$services[$i];
       $html=$html."    Ucaml:: service('".$service["name"]."',\n".parametersToUcaml($service,"      ")."),\n";
   }
    $html=rtrim($html, ", ");
   return $html;
}

/**
 * Download the ucaml file in the client side
 *
 * @param $url
 * @param $string
 * @param $format
 */
function downloadUcaml($url,$string,$format){
    $textfile=fopen($url,'w+');
    fwrite($textfile,$string);
    fclose($textfile);
    $file=fopen($url,'r');
    header("Content-Type: text/plain");
    header("Accept-Ranges: bytes");
    header("Accept-Length: ".filesize($url));
    header("Content-Disposition: attachment; filename=".$format."");
    echo fread($file, filesize($url));
    fclose($file);
}

