//Check application name if is empty
$(".application-name").on("blur",function () {
    checkEmpty(this);
});

//Check service name if is empty
$("body").on("blur",".service-name", function () {
    checkEmpty(this);
});

//Check the format of ports
$("body").on("blur",".ports", function () {
    checkInteger(this);
});

//Check container name if is empty
$("body").on("blur",".container-name", function () {
    checkEmpty(this);
});

/**
 * Check whether input is empty
 *
 * @param obj
 * @returns {boolean}
 */
function checkEmpty(obj){
    let tr=$(obj).parents("tr");
    if(obj.value!==""){
        if(tr.next(".hint-tr").exist()){
            tr.next(".hint-tr").remove();
        }
        return true;
    }
    else if(!tr.next(".hint-tr").exist()){
        tr.after("<tr class='hint-tr'><td></td><td class='hint-td'>This item can't be empty.</td></tr>");
        return false;
    }

}

/**
 * Validate the current form
 *
 * @returns {boolean}
 */
function validateCurrentTable(tableClass){
    let form=$(tableClass);
    let inputs=form.find("input");
    for(let i=0; i<inputs.length; i++){
        let input=inputs.eq(i);

        //Check if all necessary blanks except whose place holder contains optional are empty.
        if(!input.hasClass("ephemeral_storage") && !input.hasClass("nfs") && !input.hasClass("git") && !input.hasClass("secret") && !input.hasClass("mountPath")&& !input.hasClass("cmd") && !input.hasClass("targetCpu") && inputs.eq(i).val()===""){
            if(form.prev(".end-hint").exist()|| form.next(".end-hint").exist()){
                return false;
            }
            else {
                input.css("border-color","red");
                if(tableClass===".tab-application"){
                    form.after("<div class='end-hint'><p>Pleas input all necessary blanks correctly except whose place holder contains optional. If you want to discard some parameters, please delete them with the minus icon.</p></div>");
                }
                else if(tableClass===".tab-service"){
                    form.before("<div class='end-hint'><p>Pleas input all necessary blanks correctly except whose place holder contains optional. If you want to discard some parameters, please delete them with the minus icon.</p></div>");
                }
                return false;
            }
        }
    }

    //Check if the format of all parameters are correct.
    if(form.find(".hint-tr").length!==0){
        if(form.prev(".end-hint").exist()|| form.next(".end-hint").exist()){
            return false;
        }
        else {
            if(tableClass===".tab-application"){
                form.after("<div class='end-hint'><p>Pleas input all necessary blanks correctly except whose place holder contains optional. If you want to discard some parameters, please delete them with the minus icon.</p></div>");
            }
            else if(tableClass===".tab-service"){
                form.before("<div class='end-hint'><p>Pleas input all necessary blanks correctly except whose place holder contains optional. If you want to discard some parameters, please delete them with the minus icon.</p></div>");
            }
            return false;
        }
    }

        $(".end-hint").each(function(){$(this).remove()});
        inputs.each(function (){$(this).css("border-color","black")});
    return true;
}

/**
 * Check whether is integer and some format according to specific requirement
 * parameter targetCpu (0,100)
 * parameter ports (1,65535)
 *
 * @param obj the input that need to be checked
 * @returns {boolean}
 */
function checkInteger(obj){
    let input=obj.value;
    let tr=$(obj).parents("tr");
    if(/^\d+$/.test(input) || input===""){
       if(tr.next(".hint-tr").exist()){
           tr.next(".hint-tr").remove();
       }

       if($(obj).hasClass("targetCpu")){
          if(parseInt(input)<100 || input===""){
              if(tr.next(".hint-tr").exist()){
                  tr.next(".hint-tr").remove();
              }
          }
          else if(!tr.next(".hint-tr").exist()) {
              tr.after("<tr class='hint-tr'><td></td><td class='hint-td'>Target CPU's range is (0,100).</td></tr>");
          }
       }

       if($(obj).hasClass("ports") || $(obj).hasClass("pre") || $(obj).hasClass("new")){
           if(parseInt(input)<65535 && parseInt(input)>1 ){
               if(tr.next(".hint-tr").exist()){
                   tr.next(".hint-tr").remove();
               }
           }
           else if(!tr.next(".hint-tr").exist()) {
               tr.after("<tr class='hint-tr'><td></td><td class='hint-td'>Port's range should be (1,65535).</td></tr>");
           }
       }
       return true;
    }
    else if( !tr.next(".hint-tr").exist()){
        tr.after("<tr class='hint-tr'><td></td><td class='hint-td'>This item can only be positive integer</td></tr>");
        return false;
    }
}

//Check if a Jquery object if exists
(function($) {
    $.fn.exist = function(){
        if($(this).length>=1){
            return true;
        }
        return false;
    };
})(jQuery);

