let parentTable;
let classData;
let tr;
let serviceIndex;

//open template window
$("body").on("click",".glyphicon-tag", function () {
    openMcover(this);
});

//save as ucaml and export it to docker format
$(".submit #toDocker").on("click",function () {
    formSubmit(this);
});

//save as ucaml and export it to K8s format
$(".submit #toK8s").on("click",function () {
    formSubmit(this);
});

//Save a template
$("#template-name-form .save-template").on("click",function () {
    saveTemplate();
});

//Apply a templat
$("#template-apply .apply-template").on("click",function () {
    applyTemplate();
});






/**
 * Open template window and load the templates list into form from server.
 *
 * @param obj
 */
function openMcover (obj){
    let templateSelection = $("#template-list").empty();
    parentTable = $(obj).parents("table");
    tr=$(obj).parents("tr");
    serviceIndex=tr.find("input").attr("class").replace(/[^0-9]/ig,"");

    // Target template is service
    if(tr.hasClass("service")){
       let how="getService";
       getTemplateFromSession(how,"");
       for(key in classData){
           templateSelection.append("<option value=" + key + ">" + key + "</option>")
       }
   }

   // Target template is parameter
   else {
       let templateClass = tr.attr("class").split("-")[0];
       let how = "get";
       getTemplateFromSession(how,templateClass);
       for (let i = 0; i < classData.length; i++) {
           let templateName = classData[i][0];
           templateSelection.append("<option value=" + templateName + ">" + templateName + "</option>")
       }
    }
}

/**
 *Connect to server with ajax and get template from session
 *
 * @param how  get parameter template or service template
 * @param templateClass if template is parameter, which class this template is
 */
function getTemplateFromSession(how,templateClass){
    $.ajax({
        url: "php/Model.php",
        type: "POST",
        data: {
            how:how,
            templateClass:templateClass,
        },
        dataType:"json",
        error: function ()
        {
            alert("Failed to get service template");
        },
        async: false,
        success: function (data)
        {
            displayTemplatePage();
            classData=data;
        }
    });
}


/**
 * Close template window
 */
function closeMcover() {
    $('#mcover').css("display","none");
}

/**
 *Connect to server and save template to session
 */
function saveTemplate(){
    let templateName = $(".template-name").val();
    let dataArray = new Array();

    //Template is service
    if(tr.hasClass("service")){
        let form=$(".whole-form");
        let how="saveService";
        $.ajax({
            url: "php/Model.php?how="+how+"&templateName="+templateName+"&serviceIndex="+serviceIndex,
            type: "POST",
            data: form.serialize(),
            error: function ()
            {
                alert("failed");
            },
            success: function (data)
            {
                alert(data);
            }
        });
    }

    //Template is parameter
    else {
        let input = parentTable.find("." + templateClass);
        let how = "save";
        dataArray[0] = templateName;
        for (let i = 1; i < input.length + 1; i++) {
            dataArray[i] = input.eq(i - 1).val();
        }
        $.ajax({
            type: "POST",
            url: "php/Model.php",
            data: {
                how: how,
                templateClass: templateClass,
                dataArray: dataArray
            },
            success: function () {
                alert("Template was saved!");
            },
            async: false,
            error: function () {
                alert("Request Failed");
            }
        });

    }
    closeMcover();
}

/**
 * Fill the target form with template
 */
function applyTemplate() {
    let selection=$("#template-list").val();

    //The template is a service
    if(tr.hasClass("service")) {
        let service = classData[selection];
        let serviceForm = $("#service" + serviceIndex);
        tr.find("input").val(service['name']);
        fixedParameterToForm(serviceForm,service);
        for (key in service) {
            if (service.hasOwnProperty(key)) {
                //if parameter doesn't exist, add one.
                if (serviceForm.find("." + key).length === 0) {
                    let obj = serviceForm.find(".app-opt");
                    obj.value = key;
                    addParameter(obj);
                }

                if (key === "labels" || key === "scheduling" || key === "environment" || key === "capabilities" || key === "expose") {
                    if (key === "expose") {
                        kvParameterToForm(serviceForm, service, key, "pre", "new");
                    }
                    else {
                        kvParameterToForm(serviceForm, service, key, "key", "value")
                    }
                }

                if (key === "scale" || key === "request") {
                   scaleOrRequestToForm(serviceForm,service[key],key);
                }
                if(key==="volume"){
                    volumeToForm(serviceForm,service[key]);
                }
            }
        }
    }

    //The template is a parameter
    else {
        let input = parentTable.find("." + templateClass);
        for (let i = 0; i < classData.length; i++) {
            if (classData[i][0] === selection) {
                if (input.length < classData[i].length - 1) {
                    let count = classData[i].length - 1 - input.length;
                    let cl = input.eq(0);
                    while (count > 0) {
                        addItem(cl);
                        count = count - 2;
                    }
                }
                for (let a = 1; a < classData[i].length; a++) {
                    input = parentTable.find("." + templateClass);
                    input.eq(a - 1).val(classData[i][a]);
                }
            }
        }
    }
    closeMcover();
}


/**
 * submit the form
 */
function formSubmit(obj) {
    let form=$(".whole-form");
    if(validateCurrentTable(".tab-application")){
        if($(obj).attr("id")==="toK8s"){
            form.attr("action","php/toUcaml.php?way=K8s");
        }
        else{
            form.attr("action","php/toUcaml.php?way=Docker");
        }
        form.submit();
        alert("Submit Successfully!");
    }
}






