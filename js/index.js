//initiation: provide first service for application
let tabService = $(".tab-service");
let serviceCount=0;
addService();

let body=$("body");

//Back to application layer
$("#form .back").on("click",function () {
    backToApplication();
});

//open load file window
$(".submit #load-button").on("click",function () {
    openLoadWindow();
});

//Turn to edit template name window
$("#option-page .save-template").on("click",function () {
    turnTo('template-name-form');
});

//Turn to template list window
$("#option-page .apply-template").on("click",function () {
    turnTo('template-apply');
});

//Close template window
$("#mcover .glyphicon-remove-sign").on("click",function () {
    $('#mcover').css("display","none");
});

//Load a ucaml json
$("#load .upload-button").on("click",function () {
    loadUcamlFile();
});

//Close load file window
$("#load .glyphicon-remove-sign").on("click",function () {
    $("#load").css("display","none");
});

//add a optional parameter
body.on("change"," .app-opt", function () {
    addParameter(this);
});

//add a extra key item for parameter
body.on("click",".glyphicon-plus-sign", function () {
    addItem(this);
});

//Go to service layer
body.on("click",".to-service-icon", function () {
    toService(this);
});



//plus icon, which can add one more item
const plus="<span class='glyphicon glyphicon-plus-sign'></span>";
//minus icon, which can delete the whole data of this parameters
const minus="<span class='glyphicon glyphicon-minus-sign' onclick='deleteRow(this)'></span>";
//tag icon, which help user to create and reuse template
const tag="<span class='glyphicon glyphicon-tag'></span>";
const deleteItem="<span class='glyphicon glyphicon-remove' onclick='deleteRow(this)'></span>";

/**
 * Add a new parameter who takes a whole row in a table
 *
 * @param trClass the class that row belongs to.
 * @param name  the name of this row
 * @param input1  one of inputs in this row
 * @param input2  one of inputs in this row
 * @param plus
 * @param minus
 * @param tag
 * @returns {string} the html
 */
function newPara(trClass,name, input1, input2, plus, minus, tag){
return "<tr class="+trClass+"><td>"+name+":</td><td>"+input1+"</td><td>"+input2+"</td><td></td><td>"+plus+"</td><td>"+minus+"</td><td>"+tag+"</td></tr>";
  
}

/**
 * Add an item for specific parameter, this item has only one input and takes a whole row in table
 *
 * @param paraClass the class of parameters
 * @param name The name of parameters
 * @param input
 * @param plus
 * @param minus
 * @param tag
 * @returns {string}
 */
function newItem(paraClass,name, input, plus, minus,tag){
  return "<tr class='"+paraClass+" sub' ><td>"+name+"</td><td colspan='2'>"+input+"</td><td></td><td>"+plus+"</td><td>"+minus+"</td><td>"+tag+"</td></tr>";
}

/**
 * Add an item for specific parameter, this item has two inputs and takes a whole row in table
 *
 * @param paraClass
 * @param name
 * @param input1
 * @param input2
 * @param plus
 * @param minus
 * @returns {string}
 */
function newItem2(paraClass,name, input1, input2, plus, minus){
return "<tr class='"+paraClass+" sub' ><td>"+name+"</td><td>"+input1+"</td><td>"+input2+"</td><td></td><td>"+plus+"</td><td>"+minus+"</td><td></td></tr>";
  
}

/**
 * Define a input element for html
 *
 * @param name
 * @param className
 * @param placeholder
 * @param validation
 * @returns {string}
 */
function newInput(name, className, placeholder, validation) {
    return "<input name=" + name + " type='text' class='"+className+"' placeholder=" + placeholder + " onblur='"+validation+"'>";
}

/**
 * Add a item for current parameter
 *
 * @param obj
 */

function addItem(obj){
  let element=$(obj).parents("tr");
  let cNode = element.clone();
  let className=element.attr("class");
  let siblings=element.siblings("."+className+"");
  let children=cNode.children();
  children.first().text("");
  children.eq(4).text("");
  children.eq(3).text("");
    if(className==="service"){
      children.eq(4).append(deleteItem);
      cNode.find("input").attr({"name":"services["+serviceCount+"][name]","class":"service"+serviceCount});
      addService();
    }
    else{
       children.eq(5).text("");
       children.eq(6).text("");
       children.eq(5).append(deleteItem);
    }
  cNode.find("input").val("");
  cNode.addClass("sub");
  if(siblings.length===0){
    element.after(cNode);
  }
  else{
  siblings.last().after(cNode);
  }
}


/**
 * Delete an extra item or delete a whole parameter
 *
 * @param obj
 */
function deleteRow(obj){
  let tr=$(obj).parents("tr");
  let className=tr.attr("class");
  let siblings=tr.siblings("."+className+"");

  //Delete an extra item
  if(tr.hasClass("sub")){
    if(tr.hasClass("service")){
     let serviceId=tr.find("input").attr("class");
     $("#"+serviceId+"").remove();
    }
    tr.remove();
  }

  //Delete the whole parameter, including its extra parameters.
  else{
    siblings.remove();
    tr.remove();
  }
}


/**
 * Add a optional parameter
 *
 * @param obj
 */
function addParameter(obj){
  let selection=obj.value;
  let name=selection.substring(0,1).toUpperCase()+selection.substring(1);
  let html;
  let input1;
  let input2;
  let tableId=$(obj).parents("table").attr("id");
  let parent;
  let trClass=selection+"-tr";

  if(tableId==="tab-application"){
     parent=selection;
  }
  else{
    parent="services["+tableId.replace(/[^0-9]/ig,"")+"]"+"["+selection+"]";
  }
  
  //Parameters which are key value pairs
  if(selection==="labels" || selection==="scheduling" || selection==="environment"|| selection==="capabilities"){
    input1=newInput(parent+"[key][]",selection,"Key","");
    input2=newInput(parent+"[value][]",selection,"Value", "");
    html=newPara(trClass,name,input1,input2, plus, minus, tag);
  }
 
  if(selection==="request"){
    input1=newInput(parent+"[cpu]",selection,"CPU/Millicores","checkInteger(this)");
    input2=newInput(parent+"[memory]",selection+" memory","Memory/Megabytes","checkInteger(this)");
    let input3=newInput(parent+"[ephemeral_storage]",selection+" ephemeral_storage","Ephemeral_storage(optional)/Gigabytes","checkInteger(this)");
    html=newPara(trClass,name,input1,input2,"", minus, tag)+newItem(trClass,"",input3,"", "","");
  }
  
  if(selection==="scale"){
    input1=newInput(parent+"[min]",selection,"Minimum deployment units","checkInteger(this)");
    input2=newInput(parent+"[max]",selection,"Maximum deployment units", "checkInteger(this)");
    let optInput1=newInput(parent+"[targetCpu]",selection+" targetCpu","targetCPU(optional)","checkInteger(this)");
    html=newPara(trClass,name,input1,input2,"", minus, tag)+newItem(trClass,"",optInput1,"", "","");
}
  
  if(selection==="volume"){
      input1=newInput(parent+"[name]",selection+" name","Name", "checkEmpty(this)");
      input2="<select name='"+parent+"[type]' class='"+selection+" type'> <option>emptydir</option><option >nfs</option>" +
          "<option>git</option><option>inmemory</option><option>secret</option></select>";
      let optInput1=newInput(parent+"[nfs]",selection+" nfs","nfs(optional)","");
      let optInput2=newInput(parent+"[git]",selection+" git","git(optional)","");
      let optInput3=newInput(parent+"[secret]",selection+" secret","secret(optional)","");
      let optInput4=newInput(parent+"[mountPath]",selection+" mountPath","mountPath(optional)","");
      html=newPara(trClass,name,input1,input2,"", minus, tag)+newItem2(trClass,"",optInput1,optInput2,"", "")+newItem2(trClass,"",optInput3,optInput4,"","");
  }
    
  if(selection==="expose"){
    input1=newInput(parent+"[key][]",selection+" pre","Present-port","checkInteger(this)");
    input2=newInput(parent+"[value][]",selection+" new","New-port","checkInteger(this)");
    html=newPara(trClass,name,input1,input2,plus, minus, tag);
}

    if($("#"+tableId).find("."+selection).length===0) {
        if (tableId === "tab-application") {
            $(".tab-application").append(html);
        }
        else {
            $(obj).parents("table").find(".container-tr").before(html);
        }
    }
    else{
      alert("This parameters exists.");
    }
}

/**
 * Add a service page
 */
function addService () {
  let tabServiceCopy = tabService.clone();
  let serviceName="services["+serviceCount+"]";
  let serviceId="service"+serviceCount;
   tabServiceCopy.attr({"id":serviceId});
   tabServiceCopy.find(".ports").attr("name",serviceName+"[ports][]");
   tabServiceCopy.find(".container-name").attr("name",serviceName+"[container][name]");
   tabServiceCopy.find(".image").attr("name",serviceName+"[container][image]");
   tabServiceCopy.find(".cmd").attr("name",serviceName+"[container][cmd][]");
   tabServiceCopy.css("display","none");
   $(".tab-application").after(tabServiceCopy);
   serviceCount++;
}

/**
 * go to corresponding service page
 * @param obj
 */
function toService(obj){
  let input=$(obj).parents("tr").find("input");
  let serviceId="#"+input.attr("class");
  let serviceName=input.val();
  if(serviceName!=="") {
      $(serviceId).find(".service-name").text(serviceName);
      $(".tab-application").css({"display": "none"});
      $(serviceId).css("display", "inline-block");
      $(serviceId).addClass("current-service");
      $("#form header .title").text("Service layer");
      $("#form header .back, .right").css({"display": "inline-block"});
      $("#form .submit").css({"display": "none"});
      $("#icon-container .application").attr("src","images/application.png");
      $("#icon-container .service").attr("src","images/serviceRed.png");
      $("#icon-container .container").attr("src","images/containerRed.png");
  }
  else{
      alert("Please input service's name!");

  }
  
}

/**
 * Back to application layer
 */
function backToApplication(){
if(validateCurrentTable(".current-service")) {
    $(".current-service").removeClass("current-service");
    $(".tab-application").css({"display": "inline-block"});
    $(".tab-service").css({"display": "none"});
    $("#form header .title").text("Application layer");
    $("#form header .back, .right").css({"display": "none"});
    $("#form .submit").css({"display": "inline-block"});
    $("#icon-container .application").attr("src","images/applicationRed.png");
    $("#icon-container .service").attr("src","images/service.png");
    $("#icon-container .container").attr("src","images/container.png");
}
}

/**
 * Turn to the page whose name is pageId
 *
 * @param pageId
 */
function turnTo(pageId){
    $('#option-page').css("display","none");
    $("#"+pageId).css("display","block");
}

/**
 * Display template window
 */
function displayTemplatePage() {
    $('#mcover').css("display", "block");
    $("#template-apply").css("display", "none");
    $("#template-name-form").css("display", "none");
    $('#option-page').css("display", "block");
    $(".template-name").val("");
}

document.getElementById("file").onchange = function() {
    document.getElementById("userdefinedFile").value = document.getElementById("file").value;
};


//Animation for navigation bar
$(function(){
    let nav=$("#main-navigation");
    let win=$(window);
    let sc=$(document);
        win.scroll(function(){
            if(sc.scrollTop()>=50){
                nav.addClass("navbar-fixed-top");
                $("#main-navigation .container").css({"border-bottom":"1.5px solid #757575"});
                $(".navTemp").hide(1000);

            }else{
                nav.removeClass("navbar-fixed-top");
                $("#main-navigation .container").css({"border-bottom":"0"});
                $(".navTemp").show(1000);

            }
        })
});

/**
 * Open load window
 */
function openLoadWindow() {
    $("#load").css("display","block");
    $("#userdefinedFile").val("");
}


/**
 *Load a ucaml file to the form
 */
function loadUcamlFile () {
    $("#load").css("display","none");
    let selectedFile = document.getElementById('file').files[0];
    let reader=new FileReader();
    reader.readAsText(selectedFile);
    reader.onload=function() {
        let json=JSON.parse(this.result);

        //clear all data in this form
        $(".whole-form")[0].reset();
        $(".tab-application").find("tr").each(function(){
           if(!($(this).find("input").hasClass("application-name") || $(this).hasClass("service")||$(this).find("select").hasClass("app-opt"))){
               $(this).remove();
           }
        });
        $(".tab-application").find(".service").each(function(){
            if($(this).find("input").attr("class")!=="service0"){
                $(this).remove();
            }});
        $(".tab-service[id!='original-service']").each(function(){$(this).remove();});
        serviceCount=0;
        addService();

        //Load the json file to the form
        $(".application-name").val(json["name"]);
        ucamlParametersToForm(json,$(".tab-application"));

        //load all services to the form
        let a = 0;
        do {
            let service = json["services"][a];
            let serviceForm = $("#service" + a);
            $(".tab-application").find(".service" + a).val(service['name']);
            fixedParameterToForm(serviceForm, service);
            ucamlParametersToForm(service,serviceForm);
            a++;
            if (a !== json["services"].length) {
                addItem($(".service0"));
            }
        } while (a < json["services"].length);
    };
}

/**
 * Get the length of associative array
 *
 * @param obj the array
 * @returns {number} the length
 */
function getObjLength(obj){
    let count=0;
    for(let name in obj){
        if(typeof obj[name] === "object"){
            count+=getObjLength(obj[name]);
        }else{
            count++;
        }
    }
    return count;
}

/**
 * Fill target form with parameter whose data type is key value pairs.
 *
 * @param serviceForm the form of specific service
 * @param service service object
 * @param key the key of service object
 * @param paraKey the key of parameter object
 * @param paraValue the value of corresponding parameter key
 */
function kvParameterToForm(serviceForm, service,key, paraKey,paraValue){
    let inputs = serviceForm.find("." + key);
    let count = service[key][paraKey].length - inputs.length / 2;
    for (let b = 0, c = 0; b < service[key][paraKey].length; b++, c = c + 2) {
        inputs = serviceForm.find("." + key);
        inputs.eq(c).val(service[key][paraKey][b]);
        inputs.eq(c + 1).val(service[key][paraValue][b]);
        while (count > 0) {
            addItem(inputs.eq(0));
            count--;
        }
    }
}

/**
 * load all fixed parameters of service to form
 *
 * @param serviceForm the form of specific service
 * @param service service object
 */
function fixedParameterToForm(serviceForm,service){
    serviceForm.find(".container-name").val(service['container']['name']);
    serviceForm.find(".image").val(service['container']['image']);
    cmdOrPortsToForm(serviceForm, service, "ports");
    if(service['container']["cmd"]){
        cmdOrPortsToForm(serviceForm, service['container'], "cmd");
    }
}

/**
 * Load scale or request to form
 *
 * @param serviceForm the form of specific service
 * @param parameter
 * @param key
 */
function scaleOrRequestToForm(serviceForm, parameter, key){
    let inputs = serviceForm.find("." + key);
    let a = 0;
    for (paraKey in parameter) {
        if (parameter.hasOwnProperty(paraKey)) {
            inputs.eq(a).val(parameter[paraKey]);
            a++;
        }
    }
}

/**
 * Load volume to form
 *
 * @param servieForm the form of specific service
 * @param parameter
 */
function volumeToForm(servieForm, parameter) {
    servieForm.find("."+key).eq(0).val(parameter["name"]);
    for (paraKey in parameter) {
        if (parameter.hasOwnProperty(paraKey)) {
            servieForm.find("."+paraKey).val(parameter[paraKey]);
        }
    }
}

/**
 * load cmd or ports to form
 *
 * @param serviceForm the form of specific service
 * @param service service object
 * @param parameter
 */
function cmdOrPortsToForm(serviceForm, service, parameter) {
    let inputs=serviceForm.find("."+parameter);
    let count =service[parameter].length - inputs.length;
    for(let a=0; a<service[parameter].length; a++){
        inputs=serviceForm.find("."+parameter);
        inputs.eq(a).val(service[parameter][a]);
        while(count>0){
            addItem(inputs.eq(0));
            count--;
        }
    }
}

/**
 * Load all the ucaml parameters in a service to form
 *
 * @param service
 * @param serviceForm
 */
function ucamlParametersToForm(service,serviceForm) {
    for (key in service) {
        if (service.hasOwnProperty(key)) {
            //if parameter doesn't exist, add one.
            if (serviceForm.find("." + key).length === 0) {
                let obj = serviceForm.find(".app-opt");
                if(key==="volumes"){
                    key="volume";
                }
                obj.value = key;
                addParameter(obj);
            }
            if (key === "labels" || key === "scheduling" || key === "environment" || key === "capabilities" || key === "expose") {
                let array;
                if (key === "expose") {
                    array = service[key][0];
                }
                else {
                    array = service[key];
                }
                let inputs = serviceForm.find("." + key);
                let count = getObjLength(array) - 1;

                let c = 0;
                for (paraKey in array) {
                    if (array.hasOwnProperty(paraKey)) {
                        inputs = serviceForm.find("." + key);
                        inputs.eq(c).val(paraKey);
                        inputs.eq(c + 1).val(array[paraKey]);
                        while (count > 0) {
                            addItem(inputs.eq(0));
                            count--;
                        }
                        c = c + 2;
                    }
                }
            }

            if (key === "scale" || key === "request") {
                scaleOrRequestToForm(serviceForm, service[key], key);
            }
            if(key==="volume"){
                volumeToForm(serviceForm,service["volumes"][0]);
            }
        }
    }
}






